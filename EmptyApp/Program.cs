using EmptyApp.Models;
using EmptyApp.Services;

namespace EmptyApp
{
    public class Program
    {
        public static void Main(string[] args)
        {
            var builder = WebApplication.CreateBuilder(args);            


            builder
                .Services.AddControllersWithViews()
                .Services.AddDbContext<AppDbContext>();


            builder
                .Services.AddTransient<ICourseService, CourseService>();

            builder
                .Services.AddTransient<IGroupService, GroupService>();

            builder
                .Services.AddTransient<IStudentService, StudentService>();            

            var app = builder.Build();

            app.MapControllerRoute(
                name: "default",
                pattern: "{controller=Course}/{action=Index}/{id?}");

            app.Run();
        }
    }
}