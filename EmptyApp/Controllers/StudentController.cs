﻿using EmptyApp.Models;
using EmptyApp.Services;
using Microsoft.AspNetCore.Mvc;

namespace EmptyApp.Controllers
{
    public class StudentController : Controller
    {
        private readonly IStudentService _studentService;
        private readonly IGroupService _groupService;
        

        public StudentController(IStudentService studentService)
        {
            _studentService = studentService;
        }

        public async Task <IActionResult> IndexAsync(int groupId)
        {            
            ViewBag.groupId = groupId;

            List<Student> students= await _studentService.GetByGroupIdAsync(groupId);

            return View(students);
        }

        [HttpGet]
        public IActionResult Add(int groupId)
        {
            ViewBag.groupId = groupId;

            return View();
        }

        [HttpPost]
        public async Task <RedirectResult> Add(Student student)
        {
            await _studentService.AddAsync(student);

            return Redirect($"/Student/Index/?groupId={student.GroupId}");
        }
 

        [HttpGet]
        public async Task <IActionResult> UpdateAsync(int id)
        {
            Student student = await _studentService.GetOneAsync(id);

            return View(student);
        }

        [HttpPost]
        public async Task <RedirectResult> UpdateAsync(Student student)
        {
            await _studentService.UpdateAsync(student);

            return Redirect($"/Student/Index/?groupId={student.GroupId}");
        }

        public async Task <RedirectResult> Delete (int id)
        {
            Student student = await _studentService.GetOneAsync(id);

            await _studentService.DeleteAsync(student.Id);

            return Redirect($"/Student/Index/?groupId={student.GroupId}");
        }

        public async Task<RedirectResult> GetCourseIdByGroup(int groupId)
        {
            int? courseId = await _studentService.GetCourseIdByGroupAsync(groupId);

            return Redirect($"/Group/Index/?id={courseId}");
        }
    }
}
