﻿using EmptyApp.Models;
using EmptyApp.Services;
using Microsoft.AspNetCore.Mvc;

namespace EmptyApp.Controllers
{
    public class GroupController : Controller
    {       
        private readonly IGroupService _groupService;

         public GroupController(IGroupService groupService)
        {
            _groupService = groupService;
        }

        public async Task <IActionResult> Index(Course course)
        {
            ViewBag.courseId = course.Id;

            var groupList = await _groupService.GetByCourseIdAsync(course.Id);

            return View(groupList);
        }

        [HttpGet]
        public IActionResult Add(int courseId)
        {
            ViewBag.courseId = courseId;
            return View(courseId);
        }

        [HttpPost]
        public async Task <RedirectResult> Add(Group group)
        {
            await _groupService.AddAsync(group);

            return Redirect($"/Group/Index/?id={group.CourseId}");
        }

        [HttpGet]
        public async Task <IActionResult> UpdateAsync(int id) 
        {
            Group groupToUpdate= await _groupService.GetOneAsync(id);

            return View(groupToUpdate);
        }

        [HttpPost]
        public async Task <RedirectResult> UpdateAsync(Group group)
        {
            await _groupService.UpdateAsync(group);

            return Redirect($"/Group/Index/?id={group.CourseId}");
        }

        public async Task <RedirectResult> Delete (int id)
        {
            Group group = await _groupService.GetOneAsync(id);
            
            await _groupService.DeleteAsync(group.Id);

            return Redirect($"/Group/Index/?id={group.CourseId}");
        }
        
    }
}
