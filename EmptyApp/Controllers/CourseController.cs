﻿using EmptyApp.Migrations;
using EmptyApp.Models;
using EmptyApp.Services;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.EntityFrameworkCore;
using System.Xml.Linq;

namespace EmptyApp.Controllers
{
    public class CourseController:Controller
    {
        private readonly ICourseService _courseService;

        public CourseController(ICourseService courseService)
        {
            _courseService = courseService;
        }

        public async Task<IActionResult> IndexAsync()
        {
            List<Course> courseList = await _courseService.GetAllAsync();

            return View(courseList);
        }

        [HttpGet]
        public IActionResult Add()
        {
            return View();
        }

        [HttpPost]
        public async Task <RedirectResult> AddAsync (Course course)
        {
             await _courseService.AddAsync(course);            

            return Redirect("/Course/Index");
        }

        public async Task <RedirectResult> DeleteAsync (int id)
        {
             await _courseService.DeleteAsync(id);            

            return Redirect("/Course/Index");
        }

        [HttpGet]
        public async Task <IActionResult> UpdateAsync(int id)
        {
            Course courseToUpdate = await _courseService.GetOneAsync(id);

            return View(courseToUpdate);
        }

        [HttpPost]
        public async Task<RedirectResult> UpdateAsync(Course course)
        {            
            await _courseService.UpdateAsync(course);           

            return Redirect("/Course/Index");
        }
    }
}
