﻿using EmptyApp.Migrations;
using EmptyApp.Models;
using Microsoft.EntityFrameworkCore;
using System.Runtime.InteropServices;

namespace EmptyApp.Services
{
    public class StudentService : IStudentService
    {
        private AppDbContext _db;
        public StudentService(AppDbContext dbContext)
        {
            _db = dbContext;
        }
        public async Task<int> AddAsync(Student student)
        {
            int id = 0;

            if (student != null)
            {
                await _db.Students.AddAsync(student as Student);

                await _db.SaveChangesAsync();

                id = student.Id;
            }

            return id;
        }

        public async Task<int> DeleteAsync(int id)
        {
            Student student = await _db.Students.FirstAsync(s => s.Id == id);

            _db.Students.Remove(student);

            await _db.SaveChangesAsync();

            return student.Id;
        }       

        public async Task<Student> GetOneAsync(int id)
        {
            Student student = await _db.Students.FirstAsync(s => s.Id == id);

            return student;
        }

        public async Task<bool> UpdateAsync(Student student)
        {
            bool result = false;

            if (student != null)
            {
                _db.Students.Update(student);

                await _db.SaveChangesAsync();

                result = true;
            }

            return result;
        }

        public async Task<List<Student>> GetByGroupIdAsync(int groupId)
        {
            List<Student> studentList = await _db.Students.Where(s => s.GroupId == groupId).ToListAsync();

            return studentList;
        }

        public async Task<int?> GetCourseIdByGroupAsync(int groupId)
        {
            Group group = await _db.Groups.FirstOrDefaultAsync(g=>g.Id == groupId);

            return group.CourseId;
        }
    }
}
