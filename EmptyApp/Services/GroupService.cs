﻿using EmptyApp.Models;
using Microsoft.EntityFrameworkCore;

namespace EmptyApp.Services
{
    public class GroupService : IGroupService
    {
        private readonly AppDbContext _db;

        public GroupService(AppDbContext dbContext)
        {
            _db = dbContext;
        }
        public async Task<int> AddAsync(Group group)
        {
            int groupId = 0;

            if (group != null)
            {
                await _db.Groups.AddAsync(group);

                await _db.SaveChangesAsync();

                groupId = group.Id;
            }

            return groupId;
        }

        public async Task<bool> DeleteAsync(int id)
        {
            Group groupToRemove = await GetOneAsync(id);

            bool isGroupStudents = await _db.Students.AnyAsync(x => x.GroupId == groupToRemove.Id);

            if (!isGroupStudents)
            {
                _db.Groups.Remove(groupToRemove);

                await _db.SaveChangesAsync();
            }

            return !isGroupStudents;
        }

        public async Task<List<Group>> GetByCourseIdAsync(int courseId)
        {
            List<Group> groupList = await _db.Groups.Where(g => g.CourseId == courseId).ToListAsync();

            return groupList;
        }        

        public async Task<Group> GetOneAsync(int id)
        {
            Group group = await _db.Groups.FirstOrDefaultAsync(x => x.Id == id);

            return group;
        }

        public async Task<bool> UpdateAsync(Group group)
        {
            bool result = false;

            if (group != null)
            {
                _db.Groups.Update(group);

                await _db.SaveChangesAsync();

                result = true;
            }

            return result;
        }
    }
}
