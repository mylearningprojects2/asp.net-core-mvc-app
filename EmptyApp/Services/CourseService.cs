﻿using EmptyApp.Models;
using Microsoft.EntityFrameworkCore;

namespace EmptyApp.Services
{
    public class CourseService : ICourseService
    {
        private readonly AppDbContext _db;

        public CourseService(AppDbContext dbContext)
        {
            _db = dbContext;
        }

        public async Task<Course> GetOneAsync(int id)
        {
            Course course = await _db.Courses.FirstOrDefaultAsync(x => x.Id == id);

            return course;
        }

        public async Task<List<Course>> GetAllAsync()
        {
            List<Course> courseList = await _db.Courses.ToListAsync();

            return courseList;
        }

        public async Task<int> AddAsync(Course course)
        {
            int courseId = 0;

            if (course!=null)
            {
                await _db.Courses.AddAsync(course);

                await _db.SaveChangesAsync();

                courseId= course.Id;
            }

            return courseId;
        }

        public async Task<bool> DeleteAsync(int id)
        {         
            Course courseToRemove = await GetOneAsync(id);

            bool isCourseGroups = await _db.Groups.AnyAsync(x => x.CourseId == courseToRemove.Id);

            if (!isCourseGroups)
            {
                _db.Courses.Remove(courseToRemove);

                await _db.SaveChangesAsync();
            }            

            return !isCourseGroups;
        }

        public async Task<bool> UpdateAsync(Course course)
        {
            bool result = false;

            if (course != null)
            {
                _db.Courses.Update(course);

                await _db.SaveChangesAsync();

                result = true;
            }

            return result;
        }
    }
}
