﻿using EmptyApp.Models;

namespace EmptyApp.Services
{
    public interface IGroupService
    {
        Task<int> AddAsync(Group group);
        Task<bool> DeleteAsync(int id);
        Task<Group> GetOneAsync(int id);
        Task<List<Group>> GetByCourseIdAsync(int relativeId);
        Task<bool> UpdateAsync(Group group);
    }
}