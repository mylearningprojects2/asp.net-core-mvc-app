﻿using EmptyApp.Models;

namespace EmptyApp.Services
{
    public interface ICourseService
    {
        public Task<int> AddAsync(Course course);

        public Task<bool> DeleteAsync(int id);

        public Task<bool> UpdateAsync(Course course);

        public Task<Course> GetOneAsync(int id);

        public Task<List<Course>> GetAllAsync();
    }
}
