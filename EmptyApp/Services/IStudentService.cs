﻿using EmptyApp.Models;

namespace EmptyApp.Services
{
    public interface IStudentService
    {
        Task<int> AddAsync(Student student);
        Task<int> DeleteAsync(int id);
        Task<Student> GetOneAsync(int id);
        Task<List<Student>> GetByGroupIdAsync(int relativeId);
        Task<bool> UpdateAsync(Student student);
        Task<int?> GetCourseIdByGroupAsync(int groupId);
    }
}