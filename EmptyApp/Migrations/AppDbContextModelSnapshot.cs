﻿// <auto-generated />
using System;
using EmptyApp.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Infrastructure;
using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Storage.ValueConversion;

#nullable disable

namespace EmptyApp.Migrations
{
    [DbContext(typeof(AppDbContext))]
    partial class AppDbContextModelSnapshot : ModelSnapshot
    {
        protected override void BuildModel(ModelBuilder modelBuilder)
        {
#pragma warning disable 612, 618
            modelBuilder
                .HasAnnotation("ProductVersion", "7.0.0")
                .HasAnnotation("Relational:MaxIdentifierLength", 128);

            SqlServerModelBuilderExtensions.UseIdentityColumns(modelBuilder);

            modelBuilder.Entity("EmptyApp.Models.Course", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd()
                        .HasColumnType("int");

                    SqlServerPropertyBuilderExtensions.UseIdentityColumn(b.Property<int>("Id"));

                    b.Property<string>("Description")
                        .HasColumnType("nvarchar(max)");

                    b.Property<string>("Name")
                        .IsRequired()
                        .HasColumnType("nvarchar(max)");

                    b.HasKey("Id");

                    b.ToTable("Courses");

                    b.HasData(
                        new
                        {
                            Id = 1,
                            Description = "Навчає як ходити на лоток",
                            Name = "Виховання котеек"
                        },
                        new
                        {
                            Id = 2,
                            Description = "Котяче дозвілля іграшки та дисципліна",
                            Name = "Як приборкати домашне тигреня"
                        },
                        new
                        {
                            Id = 3,
                            Description = "Екскурсія містом та котячним місцям",
                            Name = "Де у Камянці взяти чорного кота"
                        });
                });

            modelBuilder.Entity("EmptyApp.Models.Group", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd()
                        .HasColumnType("int");

                    SqlServerPropertyBuilderExtensions.UseIdentityColumn(b.Property<int>("Id"));

                    b.Property<int?>("CourseId")
                        .HasColumnType("int");

                    b.Property<string>("Name")
                        .IsRequired()
                        .HasColumnType("nvarchar(max)");

                    b.HasKey("Id");

                    b.HasIndex("CourseId");

                    b.ToTable("Groups");

                    b.HasData(
                        new
                        {
                            Id = 1,
                            CourseId = 1,
                            Name = "FU-1"
                        },
                        new
                        {
                            Id = 2,
                            CourseId = 3,
                            Name = "SA-5"
                        },
                        new
                        {
                            Id = 3,
                            CourseId = 1,
                            Name = "RE-2"
                        },
                        new
                        {
                            Id = 4,
                            CourseId = 3,
                            Name = "FO-3"
                        },
                        new
                        {
                            Id = 5,
                            CourseId = 2,
                            Name = "NG-9"
                        });
                });

            modelBuilder.Entity("EmptyApp.Models.Student", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd()
                        .HasColumnType("int");

                    SqlServerPropertyBuilderExtensions.UseIdentityColumn(b.Property<int>("Id"));

                    b.Property<string>("FirstName")
                        .IsRequired()
                        .HasColumnType("nvarchar(max)");

                    b.Property<int?>("GroupId")
                        .HasColumnType("int");

                    b.Property<string>("LastName")
                        .IsRequired()
                        .HasColumnType("nvarchar(max)");

                    b.HasKey("Id");

                    b.HasIndex("GroupId");

                    b.ToTable("Students");

                    b.HasData(
                        new
                        {
                            Id = 1,
                            FirstName = "Вася2",
                            GroupId = 1,
                            LastName = "Пупкин2"
                        },
                        new
                        {
                            Id = 2,
                            FirstName = "Вася3",
                            GroupId = 1,
                            LastName = "Пупкин3"
                        },
                        new
                        {
                            Id = 3,
                            FirstName = "Вася4",
                            GroupId = 1,
                            LastName = "Пупкин4"
                        },
                        new
                        {
                            Id = 4,
                            FirstName = "Вася5",
                            GroupId = 1,
                            LastName = "Пупкин5"
                        },
                        new
                        {
                            Id = 5,
                            FirstName = "Вася6",
                            GroupId = 1,
                            LastName = "Пупкин6"
                        },
                        new
                        {
                            Id = 6,
                            FirstName = "Вася7",
                            GroupId = 1,
                            LastName = "Пупкин7"
                        },
                        new
                        {
                            Id = 7,
                            FirstName = "Вася8",
                            GroupId = 2,
                            LastName = "Пупкин8"
                        },
                        new
                        {
                            Id = 8,
                            FirstName = "Вася9",
                            GroupId = 2,
                            LastName = "Пупкин9"
                        },
                        new
                        {
                            Id = 9,
                            FirstName = "Вася10",
                            GroupId = 2,
                            LastName = "Пупкин10"
                        },
                        new
                        {
                            Id = 10,
                            FirstName = "Вася11",
                            GroupId = 2,
                            LastName = "Пупкин11"
                        },
                        new
                        {
                            Id = 11,
                            FirstName = "Вася12",
                            GroupId = 2,
                            LastName = "Пупкин12"
                        },
                        new
                        {
                            Id = 12,
                            FirstName = "Вася13",
                            GroupId = 3,
                            LastName = "Пупкин13"
                        },
                        new
                        {
                            Id = 13,
                            FirstName = "Вася14",
                            GroupId = 3,
                            LastName = "Пупкин14"
                        },
                        new
                        {
                            Id = 14,
                            FirstName = "Вася15",
                            GroupId = 3,
                            LastName = "Пупкин15"
                        },
                        new
                        {
                            Id = 15,
                            FirstName = "Вася16",
                            GroupId = 3,
                            LastName = "Пупкин16"
                        },
                        new
                        {
                            Id = 16,
                            FirstName = "Вася17",
                            GroupId = 4,
                            LastName = "Пупкин17"
                        },
                        new
                        {
                            Id = 17,
                            FirstName = "Вася18",
                            GroupId = 4,
                            LastName = "Пупкин18"
                        },
                        new
                        {
                            Id = 18,
                            FirstName = "Вася19",
                            GroupId = 5,
                            LastName = "Пупкин19"
                        },
                        new
                        {
                            Id = 19,
                            FirstName = "Вася20",
                            GroupId = 5,
                            LastName = "Пупкин20"
                        },
                        new
                        {
                            Id = 20,
                            FirstName = "Вася1",
                            GroupId = 5,
                            LastName = "Пупкин1"
                        });
                });

            modelBuilder.Entity("EmptyApp.Models.Group", b =>
                {
                    b.HasOne("EmptyApp.Models.Course", "Course")
                        .WithMany("Groups")
                        .HasForeignKey("CourseId");

                    b.Navigation("Course");
                });

            modelBuilder.Entity("EmptyApp.Models.Student", b =>
                {
                    b.HasOne("EmptyApp.Models.Group", "Group")
                        .WithMany("Students")
                        .HasForeignKey("GroupId");

                    b.Navigation("Group");
                });

            modelBuilder.Entity("EmptyApp.Models.Course", b =>
                {
                    b.Navigation("Groups");
                });

            modelBuilder.Entity("EmptyApp.Models.Group", b =>
                {
                    b.Navigation("Students");
                });
#pragma warning restore 612, 618
        }
    }
}
