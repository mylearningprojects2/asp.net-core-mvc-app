﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

#pragma warning disable CA1814 // Prefer jagged arrays over multidimensional

namespace EmptyApp.Migrations
{
    /// <inheritdoc />
    public partial class Init : Migration
    {
        /// <inheritdoc />
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "Courses",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Name = table.Column<string>(type: "nvarchar(max)", nullable: false),
                    Description = table.Column<string>(type: "nvarchar(max)", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Courses", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Groups",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Name = table.Column<string>(type: "nvarchar(max)", nullable: false),
                    CourseId = table.Column<int>(type: "int", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Groups", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Groups_Courses_CourseId",
                        column: x => x.CourseId,
                        principalTable: "Courses",
                        principalColumn: "Id");
                });

            migrationBuilder.CreateTable(
                name: "Students",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    FirstName = table.Column<string>(type: "nvarchar(max)", nullable: false),
                    LastName = table.Column<string>(type: "nvarchar(max)", nullable: false),
                    GroupId = table.Column<int>(type: "int", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Students", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Students_Groups_GroupId",
                        column: x => x.GroupId,
                        principalTable: "Groups",
                        principalColumn: "Id");
                });

            migrationBuilder.InsertData(
                table: "Courses",
                columns: new[] { "Id", "Description", "Name" },
                values: new object[,]
                {
                    { 1, "Навчає як ходити на лоток", "Виховання котеек" },
                    { 2, "Котяче дозвілля іграшки та дисципліна", "Як приборкати домашне тигреня" },
                    { 3, "Екскурсія містом та котячним місцям", "Де у Камянці взяти чорного кота" }
                });

            migrationBuilder.InsertData(
                table: "Groups",
                columns: new[] { "Id", "CourseId", "Name" },
                values: new object[,]
                {
                    { 1, 1, "FU-1" },
                    { 2, 3, "SA-5" },
                    { 3, 1, "RE-2" },
                    { 4, 3, "FO-3" },
                    { 5, 2, "NG-9" }
                });

            migrationBuilder.InsertData(
                table: "Students",
                columns: new[] { "Id", "FirstName", "GroupId", "LastName" },
                values: new object[,]
                {
                    { 1, "Вася2", 1, "Пупкин2" },
                    { 2, "Вася3", 1, "Пупкин3" },
                    { 3, "Вася4", 1, "Пупкин4" },
                    { 4, "Вася5", 1, "Пупкин5" },
                    { 5, "Вася6", 1, "Пупкин6" },
                    { 6, "Вася7", 1, "Пупкин7" },
                    { 7, "Вася8", 2, "Пупкин8" },
                    { 8, "Вася9", 2, "Пупкин9" },
                    { 9, "Вася10", 2, "Пупкин10" },
                    { 10, "Вася11", 2, "Пупкин11" },
                    { 11, "Вася12", 2, "Пупкин12" },
                    { 12, "Вася13", 3, "Пупкин13" },
                    { 13, "Вася14", 3, "Пупкин14" },
                    { 14, "Вася15", 3, "Пупкин15" },
                    { 15, "Вася16", 3, "Пупкин16" },
                    { 16, "Вася17", 4, "Пупкин17" },
                    { 17, "Вася18", 4, "Пупкин18" },
                    { 18, "Вася19", 5, "Пупкин19" },
                    { 19, "Вася20", 5, "Пупкин20" },
                    { 20, "Вася1", 5, "Пупкин1" }
                });

            migrationBuilder.CreateIndex(
                name: "IX_Groups_CourseId",
                table: "Groups",
                column: "CourseId");

            migrationBuilder.CreateIndex(
                name: "IX_Students_GroupId",
                table: "Students",
                column: "GroupId");
        }

        /// <inheritdoc />
        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "Students");

            migrationBuilder.DropTable(
                name: "Groups");

            migrationBuilder.DropTable(
                name: "Courses");
        }
    }
}
