﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EmptyApp.Models
{
    public class Course:Unit
    {
        //public int Id { get; set; }

        public string Name { get; set; }

        public string? Description { get; set; }

        public List<Group> Groups { get; set; }//=new List<Group>(); - нащо воно потрібно?
    }
}
