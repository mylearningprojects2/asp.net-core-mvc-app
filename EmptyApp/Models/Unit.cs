﻿using System.ComponentModel.DataAnnotations.Schema;

namespace EmptyApp.Models
{
    [NotMapped]
    public abstract class Unit
    {
        public int Id { get; set; }
    }
}
