﻿using Microsoft.EntityFrameworkCore.Metadata.Internal;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace EmptyApp.Models
{
    public class Group : Unit
    {
        //public int Id { get; set; }

        public string Name { get; set; }

        public int? CourseId { get; set; }

        public Course? Course { get; set; }

        public List<Student> Students { get; set; } //=new List<Student>(); - нащо воно потрібно?
    }
}
