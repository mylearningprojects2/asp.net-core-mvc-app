﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace EmptyApp.Models
{
    public class AppDbContext : DbContext
    {
        public DbSet<Student> Students { get; set; }
        public DbSet<Group> Groups { get; set; }
        public DbSet<Course> Courses { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder builder)
        {
            builder
                .UseSqlServer(@"Server=(localdb)\mssqllocaldb;Database=MyDB;Trusted_Connection=True");

        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.ApplyConfigurationsFromAssembly(typeof(AppDbContext).Assembly);
        }
    }

    public class StudentConfig : IEntityTypeConfiguration<Student>
    {
        public void Configure(EntityTypeBuilder<Student> builder)
        {
            builder
                .HasKey(t => t.Id);

            builder
                .HasOne(x => x.Group)
                .WithMany(y => y.Students)
                .HasForeignKey(x => x.GroupId);

            builder
                .Property(x => x.Id).ValueGeneratedOnAdd();

            builder
                .HasData(
                new Student[]
                {

                        new Student{Id=1,LastName="Пупкин2",FirstName="Вася2", GroupId=1},
                        new Student{Id=2,LastName="Пупкин3",FirstName="Вася3", GroupId=1},
                        new Student{Id=3,LastName="Пупкин4",FirstName="Вася4", GroupId=1},
                        new Student{Id=4,LastName="Пупкин5",FirstName="Вася5", GroupId=1},
                        new Student{Id=5,LastName="Пупкин6",FirstName="Вася6", GroupId=1},
                        new Student{Id=6,LastName="Пупкин7",FirstName="Вася7", GroupId=1},
                        new Student{Id=7,LastName="Пупкин8",FirstName="Вася8", GroupId=2},
                        new Student{Id=8,LastName="Пупкин9",FirstName="Вася9", GroupId=2},
                        new Student{Id=9,LastName="Пупкин10",FirstName="Вася10", GroupId=2},
                        new Student{Id=10,LastName="Пупкин11",FirstName="Вася11", GroupId=2},
                        new Student{Id=11,LastName="Пупкин12",FirstName="Вася12", GroupId=2},
                        new Student{Id=12,LastName="Пупкин13",FirstName="Вася13", GroupId=3},
                        new Student{Id=13,LastName="Пупкин14",FirstName="Вася14", GroupId=3},
                        new Student{Id=14,LastName="Пупкин15",FirstName="Вася15", GroupId=3},
                        new Student{Id=15,LastName="Пупкин16",FirstName="Вася16", GroupId=3},
                        new Student{Id=16,LastName="Пупкин17",FirstName="Вася17", GroupId=4},
                        new Student{Id=17,LastName="Пупкин18",FirstName="Вася18", GroupId=4},
                        new Student{Id=18,LastName="Пупкин19",FirstName="Вася19", GroupId=5},
                        new Student{Id=19,LastName="Пупкин20",FirstName="Вася20", GroupId=5},
                        new Student{Id=20,LastName="Пупкин1",FirstName="Вася1", GroupId=5}
                    });
        }
    }

    public class GroupConfig : IEntityTypeConfiguration<Group>
    {
        public void Configure(EntityTypeBuilder<Group> builder)
        {
            builder
                .HasKey(t => t.Id);

            builder
                .HasOne(x => x.Course)
                .WithMany(c => c.Groups)
                .HasForeignKey(x => x.CourseId);

            builder
                .HasData(
                    new Group[]
                    {
                        new Group{Id=1,Name="FU-1", CourseId=1},
                        new Group{Id=2,Name="SA-5", CourseId=3},
                        new Group{Id=3,Name="RE-2", CourseId=1},
                        new Group{Id=4,Name="FO-3", CourseId=3},
                        new Group{Id=5,Name="NG-9", CourseId=2}
                    });
        }
    }

    public class CourseConfig : IEntityTypeConfiguration<Course>
    {
        public void Configure(EntityTypeBuilder<Course> builder)
        {
            builder
                .HasKey(t => t.Id);

            builder
                .HasData(
                    new Course[]
                    {
                        new Course { Id = 1, Name = "Виховання котеек", Description = "Навчає як ходити на лоток" },
                        new Course { Id = 2, Name = "Як приборкати домашне тигреня", Description = "Котяче дозвілля іграшки та дисципліна" },
                        new Course { Id = 3, Name = "Де у Камянці взяти чорного кота", Description = "Екскурсія містом та котячним місцям" }
                    });
        }
    }



    #region //чернетка
    //    modelBuilder.Entity<Student>()                
    //        .HasData(
    //        new Student[]
    //        {

    //            new Student{Id=1,LastName="Пупкин2",FirstName="Вася2", GroupId=1},
    //            new Student{Id=2,LastName="Пупкин3",FirstName="Вася3", GroupId=1},
    //            new Student{Id=3,LastName="Пупкин4",FirstName="Вася4", GroupId=1},
    //            new Student{Id=4,LastName="Пупкин5",FirstName="Вася5", GroupId=1},
    //            new Student{Id=5,LastName="Пупкин6",FirstName="Вася6", GroupId=1},
    //            new Student{Id=6,LastName="Пупкин7",FirstName="Вася7", GroupId=1},
    //            new Student{Id=7,LastName="Пупкин8",FirstName="Вася8", GroupId=2},
    //            new Student{Id=8,LastName="Пупкин9",FirstName="Вася9", GroupId=2},
    //            new Student{Id=9,LastName="Пупкин10",FirstName="Вася10", GroupId=2},
    //            new Student{Id=10,LastName="Пупкин11",FirstName="Вася11", GroupId=2},
    //            new Student{Id=11,LastName="Пупкин12",FirstName="Вася12", GroupId=2},
    //            new Student{Id=12,LastName="Пупкин13",FirstName="Вася13", GroupId=3},
    //            new Student{Id=13,LastName="Пупкин14",FirstName="Вася14", GroupId=3},
    //            new Student{Id=14,LastName="Пупкин15",FirstName="Вася15", GroupId=3},
    //            new Student{Id=15,LastName="Пупкин16",FirstName="Вася16", GroupId=3},
    //            new Student{Id=16,LastName="Пупкин17",FirstName="Вася17", GroupId=4},
    //            new Student{Id=17,LastName="Пупкин18",FirstName="Вася18", GroupId=4},
    //            new Student{Id=18,LastName="Пупкин19",FirstName="Вася19", GroupId=5},
    //            new Student{Id=19,LastName="Пупкин20",FirstName="Вася20", GroupId=5},
    //            new Student{Id=20,LastName="Пупкин1",FirstName="Вася1", GroupId=5}
    //        });

    //    modelBuilder.Entity<Student>()
    //        .HasKey(t => t.Id);

    //    modelBuilder.Entity<Student>()
    //        .HasOne(x => x.Group)
    //        .WithMany(y=>y.Students)
    //        .HasForeignKey(x=> x.GroupId);


    //    modelBuilder.Entity<Course>()
    //        .HasData(
    //        new Course[]
    //        {
    //            new Course { Id = 1, Name = "Виховання котеек", Description = "Навчає як ходити на лоток" },
    //            new Course { Id = 2, Name = "Як приборкати домашне тигреня", Description = "Котяче дозвілля іграшки та дисципліна" },
    //            new Course { Id = 3, Name = "Де у Камянці взяти чорного кота", Description = "Екскурсія по місту та котячним місцям" }
    //        });


    //    modelBuilder.Entity<Group>()
    //        .HasData(
    //        new Group[]
    //        {
    //            new Group{Id=1,Name="FU-1", CourseId=1},
    //            new Group{Id=2,Name="SA-5", CourseId=3},
    //            new Group{Id=3,Name="RE-2", CourseId=1},
    //            new Group{Id=4,Name="FO-3", CourseId=3},
    //            new Group{Id=5,Name="NG-9", CourseId=2}
    //        });

    //    modelBuilder.Entity<Group>()
    //        .HasOne(x => x.Course)
    //        .WithMany(c => c.Groups)
    //        .HasForeignKey(c => c.CourseId);
    //}
    #endregion

}
