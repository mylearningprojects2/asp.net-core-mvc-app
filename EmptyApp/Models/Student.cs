﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EmptyApp.Models
{
    public class Student : Unit
    {
        //public int Id { get; set; }

        public string FirstName { get; set; }
        
        public string LastName { get; set; }

        public int? GroupId { get; set; }

        public Group? Group { get; set; }
    }
}
