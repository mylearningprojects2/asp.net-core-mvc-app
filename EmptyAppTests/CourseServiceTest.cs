using EmptyApp.Models;
using EmptyApp.Services;
using Moq;

namespace EmptyAppTests

{
    [TestClass]
    public class CourseServiceTest
    {

        [TestMethod]
        public void GetAllTest()
        {
            //Arrange
            var mock = new Mock<ICourseService>(); // ��������� �������� ��������� ����

            mock.Setup(m => m.GetAllAsync()).Returns(Task.FromResult(GetAllTestData()));  //��������� ������ ���� ��������, ���� ��������� ���������

            var courseServiceTestingPart = mock.Object; // ��'��� ����������



            //Act
            var taskResult = courseServiceTestingPart.GetAllAsync();

            var resultList = taskResult.Result;



            List<Course> courseList = new List<Course>();

            foreach (Course course in resultList)
                courseList.Add(course as Course);



            //Assert
            Assert.AreEqual(courseList[0].Name, "Test1");

            //Assert.AreEqual(result.Count, 3);

        }

        [TestMethod]
        [DataRow(0, "Test1")]
        [DataRow(2, "Test3")]
        public void GetOneTest(int id, string actual)
        {
            //Arrange
            var mock = new Mock<ICourseService>();

            mock.Setup(m => m.GetOneAsync(id)).Returns(Task.FromResult(GetOneTestData(id)));

            var testObject = mock.Object;

            Course result = new();


            //Act
            var course = testObject.GetOneAsync(id).Result;

            result = (Course)course;


            //Assert
            Assert.AreEqual(result.Name, actual);

        }


        //[TestMethod]
        //[DataRow(1, false)]
        //[DataRow(2, true)]
        //public void DeleteTest(int id , bool actual)
        //{
        //    //Arrange            

        //    CourseService testService = new CourseService();

        //    var mock = new Mock<ICourseService>();

        //    mock.Setup(m => m.DeleteAsync(id)).Returns(Task.FromResult(testService.GroupListIsEmpty(GetTestDataForDelete(id))));

        //    var testObject = mock.Object;

        //    //Act
        //    var result = testObject.DeleteAsync(id).Result;

        //    //Assert
        //    Assert.AreEqual(result, actual);
        //}


        private List<Group> GetTestDataForDelete(int id)
        {
            var testData = new List<Course>() {
                new Course()
                {
                    Id = 1,
                    Name = "notEmpty",
                    Description = "test desc",
                    Groups = new List<Group>() { new Group { Id = 1, CourseId = 1, Name = "testGroup" } }
                },
                new Course()
                {
                    Id = 2,
                    Name = "Empty",
                    Description = "test desc2",
                    Groups = new List<Group>()
                }
            };

            var testReturn = testData.First(x => x.Id == id).Groups.ToList();

            return testReturn;
        }
       

        private List<Course> GetAllTestData()
        {
            List<Course> coursesList = new List<Course>
            {
                new Course() { Id = 1, Name = "Test1", Description = "Tests description 1" } as Course,
                new Course() { Id = 2, Name = "Test2", Description = "Tests description 2" } as Course,
                new Course() { Id = 3, Name = "Test3", Description = "Tests description 3" } as Course,
            };

            return coursesList;
        }


        private Course GetOneTestData(int id)
        {
            List<Course> coursesList = new List<Course>
            {
                new Course() { Id = 1, Name = "Test1", Description = "Tests description 1" } as Course,
                new Course() { Id = 2, Name = "Test2", Description = "Tests description 2" } as Course,
                new Course() { Id = 3, Name = "Test3", Description = "Tests description 3" } as Course,
            };

            return coursesList[id];
        }


    }
}